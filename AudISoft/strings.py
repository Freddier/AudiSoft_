# Define all strings used in project

class Strings:
	USER_SETTINGS = 'USER_SETTINGS'
	INDICATOR_TYPE = 'indicator_type'

	class ContentType:
		ZIP = "application/zip, application/octet-stream"

	class Response:
		GENERAL_BAD_RESPONSE = 'Bad Request'