# add/extend django features
from collections import namedtuple
from datetime import datetime, time, date
from django.db import connections
from django.http import HttpResponse
from django.shortcuts import redirect, render as page_render
from numpy import int64
from pprint import pprint
import logging
from simplejson import dumps as json_encode

from apps.dashboard.models import Config

class JsonResponse(HttpResponse):
    " Custom json encoder for non native objects"
    def default_json_encoder(self, o):
        if isinstance(o, datetime):
            r = o.isoformat()
            if o.microsecond:
                r = r[:23] + r[26:]
            if r.endswith('+00:00'):
                r = r[:-6] + 'Z'
            return r
        elif isinstance(o, date):
            return o.isoformat()
        elif isinstance(o, time):
            if is_aware(o):
                raise ValueError("JSON can't represent timezone-aware times.")
            r = o.isoformat()
            if o.microsecond:
                r = r[:12]
            return r
        elif isinstance(o, int64):
            return int(o)
        else:
            try:
                dict_ = o.__dict__
                dict_.pop('_state')
                return dict_
            except Exception as e:
                raise TypeError(repr(o) + ' is not JSON serializable')

    def __init__(self, data, safe=True, **kwargs):
        if safe and not isinstance(data, dict):
            raise TypeError('In order to allow non-dict objects to be '
                'serialized set the safe parameter to False')
        kwargs.setdefault('content_type', 'application/json')
        data = json_encode(data, default=self.default_json_encoder)
        super(JsonResponse, self).__init__(content=data, **kwargs)

def log_exception(source, exception, message):
    logger = logging.getLogger(source)
    logger.error('')
    logger.error('# # # # # # # # #  <EXCEPTION> # # # # # # # # # ')
    logger.error(message)
    logger.error(exception)
    logger.error('# # # # # # # # #  </EXCEPTION> # # # # # # # # # ')

def dictfetchall(sql):
    "Perfrom custom sql select "
    connection = connections['default']
    with connection.cursor() as cursor:
        cursor.execute(sql)
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

def execute_sql(sql):
    "Perfrom custom sql"
    connection = connections['default']
    with connection.cursor() as cursor:
        cursor.execute(sql)

def namedtuplefetchall(sql):
    "Return all rows from a cursor as a namedtuple"
    connection = connections['default']
    with connection.cursor() as cursor:
        cursor.execute(sql)
        pprint('# # # # # # # # # # # # # # # # # # # # # # ')
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]

def mainpage_if_logged(func):
    def func_wrapper(request):
        if request.user.is_authenticated:
            return redirect('dashboard')
        else:
            return func(request)
    return func_wrapper

def render(request, view, page_data = {}):
    #Getting configuration
    #---------------------------------------------------------
    config = {}
    config_variables = Config.objects.all()
    for variable in config_variables:
        config[variable.key.strip(" ")] = variable.value.strip(" ")

    page_data['config'] = config
    return page_render(request,view, page_data)


def select_fields_from_query(data, fields):
    # extract named fields from an orm query select

    ls = []
    for item in data:
        row = {}
        for field in fields:
            row[field] = item.__dict__[field]
        ls.append(row)
    return ls
