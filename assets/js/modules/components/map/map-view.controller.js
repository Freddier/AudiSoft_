angular.module('MapViewModule', [])

.controller('map_view', ['$scope', 'sharedDataService.indicatorRisk', function(scope, indicatorRisk){
	scope.selected_region = '';

	var loading = {
		show : function() { scope.loading = true},
		hide : function() { scope.loading = false}
	};

	scope.getLevelColor = function(risk_value){
		return universal.colors[universal.getLevel(risk_value)];
	}

	scope.onRegionChange = function(region_name){
		var image = $('div.regions-list img');
		if(region_name){
			image.attr('src', '/static/images/map_regions/' + region_name.trim() + '.png')
			image.show();
		}else{
			image.hide();
		}
		scope.selected_region = region_name;
	}

	var load_map = function(data){
	    scope.centerMap = function(name, coords, zoom){
			scope.onRegionChange(name);
			var coordsxy = coords.split(',');
			var region_info = {name: name, zoom:zoom, center_coords: {lat: parseFloat(coordsxy[0]), long: parseFloat(coordsxy[1]) }};
    		map_view.hidePopup();
    		map_view.showRegion(region_info);
    		scope.selected_region = name;
        }

		scope.offices = data.offices;
		scope.regions = data.regions;

		//Getting the map
		var regions_data = [
			{name: 'METROPOLITANA CENTRAL', color: '#ce93d8', value: 10, center_coords: {lat:18.472467, long:-69.925593}, zoom: 13},
			{name: 'METROPOLITANA OESTE', color: '#007688', value: 20, center_coords: {lat: 18.594692, long: -70.033461}, zoom: 11},
			{name: 'METROPOLITANA NORTE', color: '#004688', value: 20, center_coords: {lat:18.855471, long:-69.750748}, zoom: 10},
			{name: 'METROPOLITANA ORIENTAL', color: '#FF5A5E', value: 40, center_coords: {lat:18.526996, long:-69.742857}, zoom: 11},
			{name: 'ZONA ESTE', color: '#edaf30', value: 50, center_coords: {lat:18.838941, long:-68.794927}, zoom: 9},
			{name: 'ZONA SANTIAGO ESTE', color: '#80cbc4', value: 80, center_coords: {lat:19.538509, long:-70.726169}, zoom: 11},
			{name: 'ZONA SANTIAGO OESTE', color: '#cd23d8', value: 10, center_coords: {lat: 19.290608, long: -70.921437}, zoom: 10},
			{name: 'ZONA NORCENTRAL', color: '#009688', value: 45, center_coords: {lat:18.987126, long:-70.527144}, zoom: 9},
			{name: 'ZONA NORDESTE', color: '#7986cb', value: 40, center_coords: {lat:19.411188, long:-70.038893}, zoom: 9},
			{name: 'ZONA NOROESTE', color: '#4caf50', value: 50, center_coords: {lat:19.673018, long:-71.119545}, zoom: 9},
			{name: 'ZONA SURESTE', color: '#90caf9', value: 80, center_coords: {lat: 18.564102, long: -70.500380}, zoom: 10},
			{name: 'ZONA SUR', color: '#90caf9', value: 80, center_coords: {lat: 18.564102, long: -71.219545}, zoom: 9},
		];

		//Showing the map
		scope.regions_data = regions_data;
		var map_view = new MapView(scope);

		scope.goHome = map_view.goHome;
		scope.highlightRegion = map_view.highlightRegion;
		var higher_region_risk = universal.getHighestValue(data.regions, 'properties.info.risk.value');
		if(higher_region_risk){
			scope.onRegionChange(higher_region_risk.properties.info.region_name.value);
			setTimeout(function() {
				scope.showHighlight(higher_region_risk);
			}, 10);
		}
	}

	//Listening for indicator type change to start loading effect
	scope.$on('config_has_changed', function(event, config) {
		loading.show();
		$('#map').html('<div id="popup" class="ol-popup"><a href="#" id="popup-closer" class="ol-popup-closer"></a><div id="popup-content"> <ul class="collection popup-map"></ul></div></div>');
		$('#popup').hide();
	});

	//Setting information to the map
	scope.$on('indicators_risk_change', function(event){
		loading.hide();
		$('#popup').show();
		//Getting risk information from service
		var ìndicators_risk_data = indicatorRisk.getIndicatorsRisk();
		load_map(ìndicators_risk_data);
	});

	loading.show();
	$('#popup').hide();
}]);
