angular.module('DashboardModule', ['DashboardApiService'])

.controller('dashboardController', ['$scope', '$rootScope', '$q', '$timeout', 'sharedDataService.indicatorRisk', 'dashboardApiService.ApiService',
function(scope, rootScope, q, timeout, indicatorRisk, ApiService){
// summary box item object
	scope.resume_box = {};
	scope.history_by_category = scope.history_by_indicator = '';
	// page settings
	scope.settings = {
	    considered_executions: config.considered_executions
	};

	scope.$on('config_has_changed', function(event, config) {
	    // receive indicator type
	    scope.settings.indicator_type = (config.indicator_type) ? 'qty' : 'amount';
	    scope.settings.considered_executions = config.considered_executions;

	    loadAllPageInformation();
	    scope.getHistory('indicator');
	    scope.getHistory('category');
	});

	scope.get_doc = function(row, srow) {
	    var url_params = '';
	    new Loading
	    	.Builder()
	    	.forDownloading()
	    	.build();

	    if (scope.choice_type == 'doc')
	        url_params = srow.file;
	    else if (scope.choice_type == 'zip')
	        // category, indicator, indicator_type
	        url_params = format('{}/{}/', row.id, srow.id);


	    setTimeout(() => {

		    new Loading
		    	.Builder()
		    	.hide()
		    	.build();
		    document.location = '/dashboard/file/' + scope.choice_type + '/' + url_params;
	    }, 700);

	}

	scope.loadModalData = function(office, iterator) {
	    var iterator_type = {
	        'categories': 'indicators',
	        'indicators': 'files'
	    };

	    var choice_type = {
	        'categories': 'zip',
	        'indicators': 'doc'
	    };

	    scope.choice_type = choice_type[iterator];
	    scope.curr_office_modal = office;
	    scope.modal_office_data = [];
	    // create custom array for storage office data

	    for (var i in office[iterator]) {
	        if (!isNaN(parseInt(i))) { // if index is numeric
	            var element = office[iterator][i];
	            scope.modal_office_data.push({
	                id: element.id,
	                name: element.name,
	                weight: (iterator == 'indicators') ? element.show_percent : element.weight_risk,
	                iterator: element[iterator_type[iterator]],
	            });
	        }
	    }

	    // finally, show modal
	    $('#offices-detail-modal').modal({
	        dismissible: false
	    });
	    // $('#offices-detail-modal').modal('open');
	}

	scope.getHistory = function(entity_type, initial) {
	    var selector = null;
	    var entity_id = 0;

	    if (entity_type == 'indicator') {
	        selector = 'incidence_by_indicator';
	        entity_id = scope.history_by_indicator;
	    } else {
	        selector = 'incidence_by_category';
	        entity_id = scope.history_by_category;
	    }

	    loadHistoryChart(entity_type, selector, entity_id, initial);}

	function handleError(error) {
		swal("Ha ocurrido un inconveniente :(", error.message, 'error');
	}

	function loadAllPageInformation() {
	    // show all preloaders
		var handleSuccess = function(response) {
			var responseData = response.data;

		    indicatorRisk.setIndicatorsRisk(responseData);
		    scope.offices_risk = responseData.offices_risk.offices;
		    scope.categories_risk = responseData.offices_risk.categories;
		    scope.resume_box = {};

		    if (scope.offices_risk) {
		        scope.offices_risk.forEach((item) => {
		            item.risk_level = universal.getLevel(item.risk);
		        });
		    }

		    var total_percent_categories_risk = scope.categories_risk.map(a => a.total_risk).reduce((a, b) => a + b, 0);

		    if (scope.categories_risk) {
		        scope.categories_risk.forEach((item) => {
		            item.percent = (item.total_risk * 100) / total_percent_categories_risk;
		            item.risk_level = universal.getLevel(item.total_risk);
		        });
		    }

		    category_risk_graph.data.datasets[0].data = responseData.offices_risk.categories.map(function(o) {
		        return o.percent.toFixed(2)
		    });
		    category_risk_graph.data.datasets[0].backgroundColor = universal.getColors(responseData.offices_risk.categories.length);
		    category_risk_graph.data.labels = responseData.offices_risk.categories.map(function(o) {
		        return o.name
		    });
		    category_risk_graph.update();

		    region_risk_graph.data.datasets[0].data = responseData.regions.map(function(o) {
		        return o.properties.info.risk.value.toFixed(2)
		    });
		    region_risk_graph.data.datasets[0].backgroundColor = universal.getColors(responseData.regions.length);
		    region_risk_graph.data.labels = responseData.regions.map(function(o) {
		        return o.properties.info.region_name.value.trim()
		    });
		    region_risk_graph.update();

		    //Getting the category with the highest risk value
		    var highest_category = universal.getHighestValue(responseData.offices_risk.categories, 'total_risk');
		    scope.resume_box.office = universal.getHighestValue(responseData.offices_risk.offices, 'risk');
		    if (highest_category) {
		        scope.resume_box.category = highest_category;
		        var highest_indicator = universal.getHighestValue(highest_category.indicators, 'weight_risk');
		        if (highest_indicator) {
		            scope.history_by_indicator = highest_indicator.id.toString();
		            scope.resume_box.indicator = highest_indicator;
		            $('#history_by_indicator').val(highest_indicator.id);
		            scope.getHistory('indicator');
		        }

		        scope.history_by_category = highest_category.id.toString();
		        $('#history_by_category').val(highest_category.id);
		        scope.getHistory('category');
		    }

		    // set resume box display data
		    for (var i in scope.resume_box) {
		        scope.resume_box[i].text_size = 20;
		    }}

    	ApiService
			.get_offices_risk()
			.then(handleSuccess)
			.catch(handleError);
	}

	var loadHistoryChart = function(type, selector, id) {

		if (!id) return;

	    var setChart = function(data) {
	        // create new chartjs ans save instance in scope
	        scope.charts[type] = new Chart(document.getElementById(selector), {
	            type: 'bar',
	            data: {
	                labels: data.columns.reverse(),
	                datasets: [{
	                    label: "",
	                    backgroundColor: universal.getColors(3),
	                    data: data.values.reverse()
	                }]
	            },
	            options: {
	                legend: {
	                    display: false
	                },
	                title: {
	                    display: false
	                },
	                scales: {
	                    yAxes: [{
	                        ticks: {
	                            beginAtZero: true
	                        }
	                    }]
	                }
	            }
	        });
	    };
	    var handleSuccess = function (response) {
            var history_data = response.data.history_data;
            if (scope.charts[type]) {
                // if saved graph instance, update it
                scope.charts[type].data.datasets[0].data = history_data.values.reverse();
                scope.charts[type].data.labels = history_data.columns.reverse();
                scope.charts[type].update();
            } else {
                // if not saved graph instance, create it
                setChart(history_data);
            }
        }

	    ApiService.get_history(type, id)
	        .then(handleSuccess)
	        .catch(handleError);}

	// extract data functions

	var getCategoriesFromOffice = (office) => $.map(office.categories, function(value, index) {
	    return [value];
	});

	var getRiskyOffices = (offices) => offices.filter((o) => o.risk > 0);

	scope.charts = {};
	scope.getHistory('indicator');
	scope.getHistory('category');

	// create charts instance
	var category_risk_graph = new Chart(document.getElementById("line-chart-sample"), {
	    type: 'bar',
	    data: {
	        labels: [],
	        datasets: [{
	            label: "",
	            backgroundColor: [],
	            data: []
	        }]
	    },
	    options: {
	        legend: {
	            display: false
	        },
	        title: {
	            display: false
	        },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true,
	                    min: 0,
	                    max: 100,
	                    callback: function(value) {
	                        return value + "%"
	                    }
	                },
	                scaleLabel: {
	                    display: true,
	                    labelString: "Percentaje"
	                }
	            }]
	        }
	    }
	});
	var region_risk_graph = new Chart(document.getElementById("doughnut-chart"), {
	    type: 'doughnut',
	    data: {
	        labels: [],
	        datasets: [{
	            label: "",
	            backgroundColor: [],
	            data: []
	        }]
	    },
	    options: {
	        title: {
	            display: false
	        },
	        tooltips: {
	            callbacks: {
	                label: function(tooltipItem, data) {
	                    //get the concerned dataset
	                    var dataset = data.datasets[tooltipItem.datasetIndex];
	                    return dataset.data[tooltipItem.index] + "%";
	                }
	            }
	        }
	    }
	});

	loadAllPageInformation();
}]);
