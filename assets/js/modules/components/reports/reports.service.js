angular.module('ReportApiService', [])

.service('ReportApiService.ApiService', function ($http) {
	this.get_executions = function (params) {
		return http.post('/dashboard/api/get_executions', params);
	}

	this.download_pdf = function (params) {
		return http.post('/dashboard/api/download_pdf', params)
	}
})
