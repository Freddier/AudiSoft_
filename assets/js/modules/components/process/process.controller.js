angular.module('ProcessModule', ['ProcessApiService'])

.controller('procesessController', ['$scope', 'processApiService.ApiService', function ($scope, ApiService) {
	$scope.root_nodes = [];
	$scope.deployed_panels = { macro_process: false, process: false };
	$scope.current_selected = { macro_process : null, process : null };
	$scope.process_nodes = $scope.subprocess_nodes = {};


	(function getRootNodes() {
		ApiService.get_root_nodes()
			.then(function (response) {
				$scope.root_nodes = response.data;
			})
	})()

	$scope.getMilliseconds = function () {
		return (new Date()).getMilliseconds();
	}

	$scope.blur = function (identifier, child_id) {
		return $scope.current_selected[identifier] != child_id
									&& $scope.deployed_panels[identifier]
	}

	$scope.hideCollapsedPanels = function () {
		$scope.deployed_panels.macro_process = false;
		$scope.deployed_panels.process = false;
		$(".collapse-process").collapse('hide');
	}

	$scope.getProcesss = function (child_id) {

		if (!$scope.deployed_panels.macro_process) {
			$scope.deployed_panels.macro_process = true;

			if ($scope.current_selected.macro_process != child_id) {
				$scope.current_selected.macro_process = child_id;
				$scope.deployed_panels.macro_process = true;
				$scope.deployed_panels.process = false;

				// hide sub processs
				$('div.collapse-sub-process').collapse('hide');

				ApiService.get_nodes(child_id)
					.catch(handleError)
					.then(handleSucess('process_nodes'));
			}
		} else {
			$scope.deployed_panels.macro_process = false;
			$scope.current_selected.macro_process = undefined;
			$scope.current_selected.process = undefined;
		}
	}

	$scope.getSubProcesss = function (child_id) {

		if (!$scope.deployed_panels.process) {
			$scope.deployed_panels.process = true;

			if ($scope.current_selected.process != child_id) {
				$scope.current_selected.process = child_id;
				$scope.deployed_panels.process = true;

				ApiService.get_nodes(child_id)
					.catch(handleError)
					.then(handleSucess('subprocess_nodes'));
			}
		} else {
			$scope.deployed_panels.process = false;
			$scope.current_selected.process = undefined;
		}
	}


	var handleSucess = function (destine) {
		return function (response) {
			$scope[destine] = response.data;
		}
	}

	var handleError = function (error) {
		swal({title : "Opps", content: "Ha ocurrido un inconveniente procesando la solicitud", icon : 'error'});
	}
}]);
