angular.module('ProcessApiService', [])

.service('processApiService.ApiService', function ($http) {
	this.get_nodes = function (params) {
		var url = format('/procesos/nodos/{}', params);
		return $http.get(url);
	}
	this.get_root_nodes = function () {
		return $http.get('/procesos/nodos');
	}
})
