angular.module('SharedDataService', [])

.service('sharedDataService.ApiService', function ($http) {
	this.change_config = function (params) {
		return http.post('/dashboard/api/change_config', params)
	}
})

.service('sharedDataService.indicatorRisk', ['$rootScope', function(rootScope){
	var indicator_risk = null;

	this.setIndicatorsRisk = function(_indicator_risk){
		indicator_risk = _indicator_risk;
		rootScope.$broadcast('indicators_risk_change');
	}

    this.getIndicatorsRisk = function () {
    	return indicator_risk;
    }
}]);
