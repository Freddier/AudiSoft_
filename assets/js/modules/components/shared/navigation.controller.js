angular.module('NavigationModule', [])

.controller('navegationController', ['$scope', '$rootScope', function(scope, rootScope){

	// AMOUNT = TRUE
	// QTY = FALSE

	scope.toogleIndicatorType = function () {
		scope.boolean_indicator_type = !scope.boolean_indicator_type;
		rootScope.$broadcast('config_has_changed', {
			indicator_type : getIndicatorValue()
		});
	}

	var getIndicatorValue = function () {
		return scope.boolean_indicator_type ? 'amount': 'qty';
	}

	var translateValue = function (expr) {
		return (expr == 'amount') ? true : (expr == 'qty') ? false : new Error('Invalid Indicador Type');
	}

	var setIndicatorValue = function (booleanValue) {
		if (booleanValue)
			return 'amount';
		else
			return 'qty';
	}

	scope.indicator_type = config.indicator_type;
	scope.boolean_indicator_type = translateValue(scope.indicator_type);
	// rootScope.indicator_type = getIndicatorValue(scope.indicator_type);
}])