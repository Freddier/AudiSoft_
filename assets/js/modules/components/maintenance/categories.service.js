angular.module('MaintenanceCategoriesServices', [])

.service('MaintenanceCategories.ApiService', function ($http) {

	this.post_add_indicator_to_category = function (params) {
		return $http.post('/mantenimientos/add_indicator_to_category', params)
	}
	this.post_delete_indicator_from_category = function (params) {
		return $http.post('/mantenimientos/delete_indicator_from_category', params)
	}
	this.post_delete_category = function (params) {
		return $http.post('/mantenimientos/delete_category/' + params)
	}
	this.post_save_indicatorcategory = function (params) {
		return $http.post('/mantenimientos/save_indicatorcategory', params)
	}
	this.get_json_categories_weight = function () {
		return $http.get('/mantenimientos/json_categories_weight');
	}
	this.get_json_indicators = function () {
		return $http.get('/mantenimientos/json_indicators');
	}
	this.get_indicator = function (params) {
		return $http.get('/mantenimientos/indicator/' + params )
	}
	this.get_indicator = function (params) {
		return $http.get('/mantenimientos/indicator/' + params )
	}
	this.post_create_category = function (params) {
		return $http.post('/mantenimientos/create_category', params);
	}
	this.post_save_categoryweight = function (params) {
		return $http.post('/mantenimientos/save_categoryweight', params)
	}
})
