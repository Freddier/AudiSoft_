angular.module('ConfigModule', [])

.controller('configController', ['$scope', 'sharedDataService.ApiService', '$rootScope' ,function(scope, ApiService, rootScope){
	scope.changing_config = false;
	scope.config = {};
	scope.config.considered_executions = parseInt(config.considered_executions);
	scope.config.risk_percent_low = risk_percents[0];
	scope.config.risk_percent_medium = risk_percents[1];
	scope.config.indicator_type = config.indicator_type;

	var toggleMenu = function(rightPosition){
		$('ul#chat-out').animate({
			right: rightPosition + '%'
		}, 200);
	}

	$('#config-button').on('click', function(){
		toggleMenu('0');
	});

	$('body').on('click', '#sidenav-overlay, a.chat-close-collapse.right', function(){
		toggleMenu('-105');
	});

	scope.changeConfig = function(){
		if(scope.changing_config) return false;

		if(!scope.config.considered_executions || scope.config.considered_executions < 1){
			alert('Debe especificar la cantidad de ejecuciones')
			return false;
		}

		if(!scope.config.risk_percent_low){
			alert('Debe especificar el porcentaje de riesgo inferior')
			return false;
		}

		if(!scope.config.risk_percent_medium){
			alert('Debe especificar el porcentaje de riesgo medio')
			return false;
		}

		if(scope.config.risk_percent_medium < scope.config.risk_percent_low){
			alert('El porcentaje de riesgo medio no puede ser inferior o igual al porcentaje de riesgo bajo')
			return false;
		}

		var config = {
			risk_percent: JSON.stringify([scope.config.risk_percent_low, scope.config.risk_percent_medium]),
			indicator_type : rootScope.indicator_type,
			considered_executions: scope.config.considered_executions
		};

		scope.changing_config = true;


		var handleSuccessResponse = function (res) {
			risk_percents = [scope.config.risk_percent_low, scope.config.risk_percent_medium];

			rootScope.$broadcast('config_has_changed', {
				indicator_type : rootScope.indicator_type,
				considered_executions : scope.config.considered_executions
			});
			scope.changing_config = false;
		}

		var handleFailedResponse = function () {
			swal({title : "Opps", content: "Ocurrió un problema al intentar guardar la configuración", icon : 'error'});
		}

		ApiService.change_config(config)
		.then(handleSuccessResponse)
		.catch(handleFailedResponse);
	}
}])

