universal = {};

Array.prototype.shuffle = function() {
  var i = this.length, j, temp;
  if ( i == 0 ) return this;
  while ( --i ) {
     j = Math.floor( Math.random() * ( i + 1 ) );
     temp = this[i];
     this[i] = this[j];
     this[j] = temp;
  }
  return this;
}

var transparentize = function(color, opacity) {
	var alpha = opacity === undefined? 0.5 : 1 - opacity;
	return Chart.helpers.color(color).alpha(alpha).rgbString();
}

var available_colors = [
	'rgb(255, 99, 132)',
	'rgb(255, 159, 64)',
	'rgb(255, 205, 86)',
	'rgb(75, 192, 192)',
	'rgb(54, 162, 235)',
	'rgb(153, 102, 255)',
	'#0097A7',
	'#7986cb',
	'#4fc3f7',
	'#2e7d32',
	'#acece6',
	'rgb(201, 50, 209)',
	'rgb(201, 25, 252)',
];

universal.getLevel = function(value){
	var low_value = risk_percents[0]; 
	var medium_value = risk_percents[1]; 
	values = {low: low_value, medium: medium_value, high: 100};
	for(var level in values){
		if(value <= values[level])
			return level;
	}
}

universal.getColors = function(colors_length){
	var _available_colors = available_colors.shuffle();
	var colors = [];
	for(var i = 0; i <= _available_colors.length; i++){
		if(i == colors_length) return colors;
		colors.push(transparentize(_available_colors[i], .3));
	}
	
	return colors;
}

//Getting the higher value of specific array of elements
universal.getHighestValue = function(data, value_constructor){
	var constructor = value_constructor.split('.');
	var highest_element = {value: 0, element: null};

	for (var i = 0; i < data.length; i++) {
		var element_value = data[i];
		
		//Gettting element value
		var value = null;
		for (var k = 0; k < constructor.length; k++) {
			value = value ? value[constructor[k]] : element_value[constructor[k]];
		}

		//Replacing new greater value
		if(value > highest_element.value){
			//Assigning the new higher value
			highest_element = {
				value: value,
				element: element_value
			}
		}
	}

	return highest_element.element;
}

universal.colors = {low: "rgba(154, 207, 145,0.64)", medium:"rgba(250, 227, 131,0.64)", high: "rgba(217, 31, 31,0.64)"};

add_module = function (name) {
	angular.module('audit_soft').requires.push(name);
}
