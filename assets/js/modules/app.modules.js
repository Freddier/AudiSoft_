angular.module("audit_soft", [
								/* Modules */
								'ConfigModule',
								'NavigationModule',
								// 'DashboardModule',
								// 'MapViewModule',
								// 'MaintenanceCategoriesModules',
								// 'ProcessModule',
								// 'ReportModule',

								// /* Services */
								'SharedDataService',
								// 'DashboardApiService',
								// 'MaintenanceCategoriesServices',
								// 'ReportApiService'
								])

.config(['$interpolateProvider','$httpProvider', function($interpolateProvider, $httpProvider) {
	// avoid conflicts between django rendering and angular
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');

	// for posting to django
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);
