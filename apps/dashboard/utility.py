from apps.dashboard.models import Config

class Utility:

	@staticmethod
	def get_config(key):
		config = Config.objects.get(key=key)   
		return config.value
