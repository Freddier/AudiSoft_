from django.conf.urls import url

from . import views

urlpatterns = [
	# views section
    url(r'^$', views.dashboard, name='dashboard'),

    # download files section
    url(r'^file/doc/(?P<file_id>[0-9]+)/$', views.download_incidences_file, name='download_incidences_file'),
    url(r'^file/zip/(?P<category_id>[0-9]+)/(?P<indicator_id>[0-9]+)/$', views.download_group_file, name='download_group_file'),

    # api section
    url(r'^api/offices_risk/?$', views.offices_risk, name='offices_risk'),
    url(r'^api/change_config$', views.change_config, name='change_config'),
    url(r'^api/get_executions$', views.get_executions, name='get_executions'),
    url(r'^api/get_history/(?P<entity_type>\w+)?/(?P<entity_id>\d+)?/$', views.get_history, name='get_history'),
    url(r'^api/download_pdf$', views.download_pdf, name='download_pdf'),
    url(r'^reports+\/$', views.reports, name='reports'),

    url(r'^test/$', views.test, name='test'),
]
