import pdfkit
from datetime import datetime
from django.http import HttpResponse
from json import loads as json_decode

from ntpath import basename
from os import path, remove as rm_file, getcwd

from AudISoft.extension import JsonResponse, render, log_exception
from AudISoft.strings import Strings
from apps.dashboard.utility import Utility
from .models import Dashboard, IncidenceFile, IndicatorData, Config, Dashboard, Category, Indicator, Office
from .submodels.office_model import OfficeModel

def test(request):
	from django.http import JsonResponse
	return JsonResponse({'test' : 'value'})

def dashboard(request):
	categories = [{'id': category.id, 'name': category.name} for category in Category.objects.all()]
	indicators = [{'id': indicator.id, 'name': indicator.code + ' ' + indicator.name} for indicator in Indicator.objects.all()]

	page_data = {'categories': categories, 'indicators': indicators}
	return render(request,'dashboard/index.html', page_data)

def download_incidences_file(request, file_id):
	''' Get incidence excel file from row id '''

	filename, file_data = Dashboard.download_incidences_file(file_id)
	content_type = "zip" if "zip" in filename else "excel"
	response = HttpResponse(file_data, content_type="application/%s, application/octet-stream" % content_type)
	response['Content-Disposition'] = 'attachment; filename="%s"' % filename
	return response

def download_group_file(request, category_id, indicator_id ):
	''' Get incidence excel files from a pattern and download a zip '''
	user_settings = request.session.get(Strings.USER_SETTINGS)
	indicator_type = user_settings.get(Strings.INDICATOR_TYPE)


	filename, group_files_data = Dashboard.get_incidence_group_files(category_id,
									indicator_id, indicator_type)
	response = HttpResponse(group_files_data, content_type=Strings.ContentType.ZIP)
	response['Content-Disposition'] = 'attachment; filename="%s"' % filename
	return response

def offices_risk(request):
	user_settings = request.session.get(Strings.USER_SETTINGS)
	indicator_type = user_settings.get(Strings.INDICATOR_TYPE)

	risk_information = OfficeModel().get_risk(indicator_type)
	res_object = {}

	offices = []
	regions = []
	for office in risk_information['offices']:
		location = office['location'].split(',')
		offices.append({
			"type":"Feature",
			"properties":{
				"Y": float(location[0]),
				"X":float(location[1]),
				"region":office['region'],
				'risk': office['risk'],
				"info":{
					"code": {"label": "Código", "value": office['code']},
					"office_name": {"label": "Oficina", "value": office['name']},
					"risk": {"label": "% Incidencias", "value": office['risk']},
					"manager": {"label": "Gerente", "value": office['manager']},
					"region": {"label": "Región", "value": office['region']},
					"type": {"label": "Tipo", "value":  office['type']},
					"address": {"label": "Dirección", "value": office['address']},
					"schedule": {"label": "Horario", "value": office['schedule']},
				}
			},
			"geometry":{
				"type":"Point",
				"coordinates":[float(location[1]), float(location[0])]
			}
		})

	for index in risk_information['regions']:
		region = risk_information['regions'][index]
		regions.append({
		    "type": "Feature",
		    "properties": {
		      "id": region['id'],
		      "center": region['center'],
		      "zoom": region['zoom'],
		      "pixel": region['pixel'],
		      "map_risk": region['map_risk'],
		      "info": {
		        "region_name": {"label": "Región","value": region['name']},
		        "risk": {"label": "% Incidencias","value": region['risk']},
		        "manager": {"label": "Encargado","value": region['manager']}
		      }
		    },
		    "geometry": {
		      "type": "Polygon",
		      "coordinates": [
		          json_decode(region['location']) if region['location'] != None else []
		      ]
		    }
		})

	# append summarized office info

	risky_offices = OfficeModel().risky_offices(risk_information['offices'])

	return JsonResponse({'offices': offices, 'regions': regions, 'offices_risk' : risky_offices})

def change_config(request):
	settings = json_decode(request.body.decode("utf-8"))
	request.session[Strings.USER_SETTINGS] = settings
	result = True

	for config_variable in settings:
		try:
			config = Config.objects.get(key=config_variable)
			config.value = settings[config_variable]
			config.save()
		except Exception as e:
			result = False
			log_exception(__name__, e, 'Fail to save config [value:{}]'.format(settings[config_variable]))

	return JsonResponse({'sucess': result})

# def get_config_data():
# 	config_variables = Config.lists.all()
# 	config = {}
# 	for variable in config_variables:
# 		config[variable.key] = variable.value

# 	return JsonResponse(config, safe=False)


def get_history(request, entity_type, entity_id):
	user_settings = request.session.get(Strings.USER_SETTINGS)
	indicator_type = user_settings.get(Strings.INDICATOR_TYPE)

	history_data = OfficeModel().get_history({
		'entity': entity_type,
		'entity_id': entity_id,
		'total_records': 3,
		'indicator_type': indicator_type
	})

	columns = ['Última Ejecución', 'Penultima Ejecución ', 'Antepenultima Ejecución']
	result = {
		'columns' : columns[:len(history_data)],
		'values' : list(map(lambda x: 0 if len(history_data) == 0 else x['total'], history_data))
	}

	return JsonResponse({'sucess': True, 'history_data': result })

def reports(request):
	data = {
		'offices': Office.objects.all(),
		'categories': Category.objects.all(),
		'indicators': Indicator.objects.all(),
	}

	return render(request,'dashboard/reports.html', data)

def get_execution_info(request):
	form = json_decode(request.body.decode("utf-8"))

	columns = data = []
	group_by = form['group_by']
	group_element = {
		'indicator_id' : {'column': 'indicator_name', 'name': 'Indicador'},
		'category_id' : {'column': 'category_name', 'name': 'Categoría'},
		'office_id' : {'column': 'office_name', 'name': 'Oficina'},
	}

	filter = {
		'offices' : form['offices'],
		'indicators' : form['indicators'],
		'categories' : form['categories'],
		'executions_count' : (int)(form['executions_count']),
		'indicator_type' :  form['indicator_type'],
		'group_by' : group_by
	}

	execution_data = OfficeModel().get_executions(filter)
	amount_description = 'Cantidad de incidencias' if filter['indicator_type'] == 'qty' else 'Monto total'

	if group_by in ['indicator_id','category_id','office_id']:
		columns = list(map(lambda c: {'title': c} , [group_element[group_by]['name'], amount_description]))
		data = list(map(lambda d: [d[group_element[group_by]['column']], "{:,}".format(d['total'])], execution_data))
	else:
		columns = list(map(lambda c: {'title': c} , ['Fecha Ejecución', 'Indicador', 'Categoría', 'Oficina', amount_description]))
		data = list(map(lambda d: [d['load_date'], d['indicator_name'], d['category_name'], d['office_name'], "{:,}".format((d['amount'] if filter['indicator_type'] == 2 else (int)(d['qty'])) )], execution_data))

	return {'columns': columns, 'data': data}

def get_executions(request):
	info = get_execution_info(request)
	return JsonResponse({'sucess': True, 'execution_data': {'columns': info['columns'], 'data': info['data']} })

def download_pdf(request):
	path_wkthmltopdf = getcwd() + '\\AudISoft\\library\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
	pdfkit.configuration(wkhtmltopdf=path_wkthmltopdf)

	form = json_decode(request.body.decode("utf-8"))
	info = get_execution_info(request)
	template = open('templates/reports/indicators_result.html')

	#Preparing report information
	template_data = []
	columns = list(map(lambda c: '<th>{0}</th>'.format(c['title']), info['columns']))
	for data in info['data']:
		row_data = list(map(lambda d: '<td>{0}</td>'.format(d), data))
		template_data.append("<tr>" + "".join(row_data) + "</tr>")

	#Getting report template
	template_info = template.read()
	template_info = template_info.replace('{base_url}', getcwd())
	template_info = template_info.replace('{report_name}', "" if 'report_name' not in form else form['report_name'])
	template_info = template_info.replace('{columns}', "".join(columns))
	template_info = template_info.replace('{info}', "".join(template_data))
	report_name   = 'Reporte de incidencias {0}.pdf'.format(datetime.now().strftime('%Y-%m-%d %H%M%S'))
	pdfkit.from_string(template_info, "dashboard/static/reports/" + report_name, configuration=config)

	return JsonResponse({'sucess': True, 'report_name': report_name}, safe=False)