from datetime import datetime
from django.db import models
from os import path
from tempfile import gettempdir
from time import localtime, strftime
from zipfile import ZipFile

# Create your models here.
class BaseModel(models.Model):
	class Meta:
		abstract = True


class Category(BaseModel):
	name = models.CharField(max_length=50, null=False)
	description = models.CharField(max_length=600, null=False)

	def __str__(self):
		return self.name


class IndicatorType(BaseModel):
	key = models.CharField(max_length=10, unique=True)
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name


class Config(BaseModel):
	key = models.CharField(max_length=50)
	name = models.CharField(max_length=50)
	value = models.CharField(max_length=100)

	def __str__(self):
		return self.name


class Indicator(BaseModel):
	code = models.CharField(max_length=50, unique=True)
	name = models.CharField(max_length=100)
	description = models.CharField(max_length=600)
	categories = models.ManyToManyField(Category, through='IndicatorCategory')

	def __str__(self):
		return self.name


class Region(BaseModel):
	name = models.CharField(max_length=50)
	area = models.TextField(max_length=400000)
	manager = models.CharField(max_length=100)
	center = models.CharField(max_length=100)
	center_zoom = models.IntegerField()
	region_pixel = models.CharField(max_length=50)

	def __str__(self):
		return self.name


class IndicatorCategory(BaseModel):
	weight = models.IntegerField()
	indicator = models.ForeignKey(Indicator, on_delete=models.PROTECT)
	category = models.ForeignKey(Category, on_delete=models.PROTECT)
	indicator_type = models.ForeignKey(IndicatorType, to_field='key', on_delete=models.PROTECT);

	def __str__(self):
		return '(category: "%s", indicator: "%s")' % (self.category.name, self.indicator.name)


class CategoryWeight(BaseModel):
	weight = models.IntegerField()
	indicator_type = models.ForeignKey(IndicatorType, to_field='key', on_delete=models.PROTECT);
	category = models.ForeignKey(Category, on_delete=models.PROTECT)


class Office(BaseModel):
	code = models.CharField(max_length=50, unique=True)
	name = models.CharField(max_length=50)
	address = models.CharField(max_length=50)
	region = models.ForeignKey(Region, on_delete=models.PROTECT)
	location = models.CharField(max_length=500)
	schedule = models.CharField(max_length=500)
	office_type = models.CharField(max_length=4)
	manager = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class IncidenceFile(BaseModel):
	path = models.CharField(max_length=300)
	base_name = models.CharField(max_length=200)
	load_date = models.DateField(max_length=50)
	indicator_code = models.ForeignKey(Indicator, db_column='indicator_code', to_field='code', on_delete=models.PROTECT);

	def has_many_files(self):
		result = True if len(self.base_name.split(';')) > 1 else False
		return result

	def get_full_path(self):
		if self.has_many_files():
			files = []
			for base_name in self.base_name.split(';'):
				files.append(path.join(self.path, base_name))
			return files
		else:
			return path.join(self.path, self.base_name)

	def get_filename(self):
		return self.base_name

	def __str__(self):
		return self.get_filename()


class IndicatorData(BaseModel):
	qty = models.DecimalField(max_digits=15, decimal_places=2)
	amount = models.DecimalField(max_digits=15, decimal_places=2)
	indicator = models.ForeignKey(Indicator,on_delete=models.PROTECT)
	attached_file = models.ForeignKey(IncidenceFile, on_delete=models.PROTECT);
	office = models.ForeignKey(Office, db_column='office_code', to_field='code', on_delete=models.PROTECT)


class Dashboard():

	@staticmethod
	def get_view(view_name, params = None):
		from AudISoft.extension import dictfetchall
		query = 'select * from {0}'.format(view_name)
		if params:
			query += ' where %s' % (' and '.join(list(map(lambda x: "%s ='%s'" % (x, params[x]), params))))
		return dictfetchall(query)

	@staticmethod
	def get_sql_data(sql):
		from AudISoft.extension import dictfetchall
		return dictfetchall(sql)

	@staticmethod
	def get_incidence_group_files(category_id, indicator_id, indicator_type_id):
		# download a group of attached incidence files

		current_time = localtime()
		# add current date/time to filename
		filename = 'Incidencias {}.zip'.format(strftime('%d-%m-%Y %H:%M%p', current_time))
		# get all attached files by indicators
		filelist = list(IncidenceFile.objects.filter(indicator_code__id=indicator_id))
		# save file in a temporary path
		zip_full_name = path.join(gettempdir(),filename)
		# zip file creator instance
		zipper = ZipFile(zip_full_name, 'w')
		# for each incidence file...
		for row in filelist:
			# ... for each semicolon named file ...
			for base_name in row.base_name.split(';'):
			# ... add to the zip file
				zipper.write(path.join(row.path,base_name), base_name)
		# close zipper instance
		zipper.close()

		file_data = None
		# read the zipped file
		with open(zip_full_name, 'rb') as f: file_data = f.read()

		return (filename, file_data)

	@staticmethod
	def download_incidences_file(file_id):
		''' Get incidence excel file from row id '''
		incidence_file_record = IncidenceFile.objects.get(pk=file_id)

		if incidence_file_record.has_many_files():
			current_time = localtime()
			# add current date/time to filename
			filename = 'Incidencias %.zip' % strftime('%d-%m-%Y %H:%M%p', current_time)
			# save file in a temporary path
			zip_full_name = path.join(gettempdir(),filename)
			# zip file creator instance
			zipper = ZipFile(zip_full_name, 'w')
			for file in incidence_file_record.get_full_path():
				zipper.write(file, basename(file))
			zipper.close()

			file_data = open(zip_full_name, 'rb').read()

			return (filename, file_data)

		else:
			filename = incidence_file_record.get_filename()
			file_data = open(incidence_file_record.get_full_path(), 'rb').read()
			return (filename, file_data)
