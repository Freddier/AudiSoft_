# from django.db import models
from apps.dashboard.models import Office, IndicatorCategory, Dashboard, IndicatorType
from apps.dashboard.sql_view_manager import ViewManager
from apps.dashboard.utility import Utility
from functools import reduce

import pandas

class OfficeModel():
	indicator_type = 0

	def sum_by_indicator(self, indicators_data):
		# Getting total amount of each indicator

		total_indicator_amount = {}
		indicator_tag = self.indicator_type
		for indicator in indicators_data:
			amount = float(indicator[indicator_tag])
			indicator_id = indicator['indicator_id']
			total_indicator_amount[indicator_id] = total_indicator_amount[indicator_id] + amount if indicator_id in total_indicator_amount else amount

		return total_indicator_amount

	def validate_indicator_type(self, indicator_type_key):
		return indicator_type_key in [x.key for x in IndicatorType.objects.all()]

	def get_indicator_risk_by_office(self, indicators_data):
		# Getting risk by indicator and office

		indicators_risk = {}
		totals = self.sum_by_indicator(indicators_data)

		for indicator in indicators_data:
			amount = float(indicator['qty']) if self.indicator_type == 1 else float(indicator['amount'])
			office_code = indicator['office_code']
			indicator_id = indicator['indicator_id']
			key = ("{0}_{1}".format(office_code, indicator_id))

			total_sum = (amount / totals[indicator_id]) if totals[indicator_id] > 0 else 0

			#Getting indicator/office risk
			if key in indicators_risk:
				indicators_risk[key]['percent'] += (amount / totals[indicator_id]) * 100 if totals[indicator_id] > 0 else 0
			else:
				indicators_risk[key] = {
					'value': amount,
					'office_code' : office_code,
					'indicator_id': indicator_id,
					'percent':  (amount / totals[indicator_id]) * 100 if totals[indicator_id] > 0 else 0
				}

		return indicators_risk

	def total_amount_by_office(self, office):
		indicators = [];

		# if not exceptions in office...
		if len(list(office['categories'].keys())) == 0:
			return 0

		for category in office['categories']:
			for indicator in office['categories'][category]['indicators']:
				# no repeat indicator on count
				if not indicator['indicator_id'] in list(map(lambda i: i['indicator_id'], indicators)):
					indicators.append(indicator);

		# totalize indicator 'value'
		# first, map for getting an array of 'value' field and
		# then reduce the array
		total_amount = reduce(lambda t, b: t + b, [x for x in map(lambda i: i['value'] , indicators)])
		return total_amount


	def risky_offices(self, office_list):
		# if not offices, we're done
		if len(office_list) == 0:
			return {}

		# offices with level of risk
		risky_offices = list(filter(lambda o: o['total_amount'] > 0, office_list))

		# if not risk detected in offices, we're done
		if len(risky_offices) == 0:
			return {}

		# order by total amount/qty of exceptions
		offices_sorted_byrisk = sorted(risky_offices, key=lambda x: x['risk'], reverse=True)

		# free unnecessary objects memory
		del office_list
		del risky_offices

		# take the first five top risky offices
		highest_offices = offices_sorted_byrisk[0:5].copy()

		# highest indicator count
		array_categories = []
		for office in offices_sorted_byrisk:
			categories = list(office['categories'].values())
			[array_categories.append(category) for category in categories]

		categories_grouped_byrisk = []

		for category in array_categories:
			id_categories_array = [c['id'] for c in categories_grouped_byrisk]
			category_index = id_categories_array.index(category['id']) if category['id'] in id_categories_array else None

			if category_index is None:
				category['total_risk'] = category['weight_risk']
				categories_grouped_byrisk.append(category)
			else:
				categories_grouped_byrisk[category_index]['total_risk'] += category['weight_risk']

		# first five top risky categories
		highest_categories = categories_grouped_byrisk[0:5].copy()
		highest_categories = sorted(highest_categories, key=lambda x: x['total_risk'], reverse=True)

		for category in highest_categories:
			indicators_sum = sum(map(lambda x: x['weight_risk'], category['indicators']))

			# TODO

			if indicators_sum == 0:
				continue

			for indicator in category['indicators']:
				indicator['show_percent'] = (indicator['weight_risk']*100)/indicators_sum

		return {
			'offices' : highest_offices,
			'categories' : highest_categories
		}

	def get_risk(self, indicator_type):
		''' get all regions and offices info  '''
		regions = {}
		office_list = []
		executions_count = Utility.get_config('considered_executions')

		if not self.validate_indicator_type(indicator_type):
			raise Exception('No valid indicator type key')

		self.indicator_type = indicator_type
		# getting category indicator by indicator type
		category_indicators = IndicatorCategory.objects.filter(indicator_type_id = self.indicator_type).all()
		# getting indicators data from las N executions
		offices_information = ViewManager().get_tree_risk(executions_count)
		# grouping information by indicator and office
		office_indicator_risk = self.get_indicator_risk_by_office(offices_information)

		# free memory
		del offices_information

		offices = Office.objects.all()
		offices_by_regions = {}

		for office in offices:
			category_count = 0
			categories_risk = {}
			sum_indicator_risk = 0

			office_object = {
				'risk': 0,
				'id': office.id,
				'categories': {},
				'code': office.code,
				'name': office.name,
				'address': office.address,
				'schedule': office.schedule,
				'region': office.region.name,
				'location': office.location,
				'type': office.office_type,
				'manager': office.manager
			}

			for category_indicator in category_indicators:
				key = ("{0}_{1}".format(office.code, category_indicator.indicator_id))

				if not key in office_indicator_risk:
					continue

				category_id = category_indicator.category_id
				indicator_id = category_indicator.indicator.id

				indicator_risk = office_indicator_risk[key]
				indicator_risk['id'] = category_indicator.indicator.id
				indicator_risk['name'] = category_indicator.indicator.name

				# getting the indicator files
				all_files_attached = category_indicator.indicator.incidencefile_set.all()
				# adding the indicator files
				indicator_risk['files'] = [
					{
						'file' : x.id,
						'path' : x.path,
						'base_name' : x.base_name,
						'name' : x.load_date
					} for x in all_files_attached ]

				#Getting category weight by indicator type
				indicator_type = self.indicator_type
				category_weight = (category_indicator.category.categoryweight_set.filter(indicator_type_id=indicator_type).values()[0])['weight']

				#Calculating category indicator risk
				category_indicator_risk =  (indicator_risk['percent'] / 100) *  category_indicator.weight
				#Calculating weight risk for indicators
				indicator_risk['weight_risk'] = category_indicator_risk
				#Calculating weight risk for categories
				weight_risk =  (category_indicator_risk / 100) * category_weight

				#Adding category indicator risk
				if category_id in categories_risk:
					categories_risk[category_id]['partial_risk'] += category_indicator_risk
					categories_risk[category_id]['weight_risk'] +=  weight_risk
				else:
				#Creating new category indicator risk
					categories_risk[category_id] = {
						'id': category_id,
						'name': category_indicator.category.name,
						'partial_risk': category_indicator_risk,
						'weight_risk': weight_risk,
						'indicators': []
					}

				categories_risk[category_id]['indicators'].append(indicator_risk.copy())
				sum_indicator_risk += weight_risk

			office_object['categories'] = categories_risk
			office_object['risk'] = sum_indicator_risk
			office_object['total_amount'] = self.total_amount_by_office(office_object)

			office_list.append(office_object.copy())
			if office.region_id in offices_by_regions:
				if office_object['risk'] > 0:
					offices_by_regions[office.region_id] += 1
			else:
				if office_object['risk'] > 0:
					offices_by_regions[office.region_id] = 1

			if office.region_id in regions:
				regions[office.region_id]['total_risk'] += office_object['risk']
				regions[office.region_id]['map_risk'] = office_object['risk'] if office_object['risk'] > regions[office.region_id]['map_risk'] else regions[office.region_id]['map_risk']
				regions[office.region_id]['risk'] = (regions[office.region_id]['total_risk'] / (1 if office.region_id not in offices_by_regions else offices_by_regions[office.region_id]) )
			else:
				regions[office.region_id] = {
					'id': office.region_id,
					'name': office.region.name,
					'manager': office.region.manager,
					'location': office.region.area,
					'total_risk': office_object['risk'],
					'risk': office_object['risk'],
					'map_risk': office_object['risk'],
					'center': office.region.center,
					'zoom': office.region.center_zoom,
					'pixel': office.region.region_pixel
				}
		return  {'offices': office_list, 'regions': regions }

	def get_history(self, filter):
		return ViewManager().get_execution_history(filter)

	def get_executions(self, filter):
		return ViewManager().get_executions(filter)