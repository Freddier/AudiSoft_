from pathlib import Path
from apps.dashboard.models import Office, IndicatorCategory, Dashboard

class ViewManager:
	root_view_path = 'dashboard\\sql_views\\'

	def get_view(self, view_name, variables = {}):
		sql_file_path = self.root_view_path + view_name + '.sql'
		sql_view = Path(sql_file_path).read_text()       
		return sql_view.format(**variables)

	def get_tree_risk(self, total_records, filter = {}):
		query_string = ' 1 = 1 '

		if 'date' in filter and 'from' in filter['date']:
			query_string += " AND di.load_date >= '{0}' ".format(filter['date']['from'])

		if 'date' in filter and 'to' in filter['date']:
			query_string += " AND di.load_date <= '{0}' ".format(filter['date']['to'])

		if 'office' in filter:
			query_string += ' AND office_code = {0}'.format(filter['office'])

		return Dashboard.get_sql_data('call risk_information_tree({0},"{1}")'.format(total_records, query_string))

	def get_execution_history(self, filter):
		entity 	       = filter['entity']
		entity_id      = filter['entity_id']
		total_records  = filter['total_records']
		indicator_type = filter['indicator_type']

		query = 'CALL execution_history("{0}", "{1}", {2}, {3});'.format(entity, indicator_type, total_records, entity_id)

		return Dashboard.get_sql_data(query)

	def get_executions(self, filter):
		group_by 	   = filter['group_by']
		indicator_type = filter['indicator_type']
		total_records  = filter['executions_count']
		record_filter = ''

		if filter['offices']:
			record_filter += (' AND office_id IN(' + ",".join([x for x in filter['offices']]) + ") ")

		if filter['indicators']:
			record_filter += (' AND indicator_id IN(' + ",".join([x for x in filter['indicators']]) + ") ")

		if filter['categories']:
			record_filter += (' AND category_id IN(' + ",".join([x for x in filter['categories']]) + ") ")				

		return Dashboard.get_sql_data('CALL executions_report("{0}", {1}, {2}, "{3}");'.format(group_by, indicator_type, total_records, record_filter))