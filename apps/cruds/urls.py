from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='crud_index'),
    url(r'^categorias$', views.category_indicator, name='crud_category_indicator'),

    url(r'^create_category$', views.create_category, name='create_category'),
    url(r'^add_indicator_to_category$', views.add_indicator_to_category, name='add_indicator_to_category'),
    url(r'^delete_category/(?P<category_id>[0-9]+)$', views.delete_category, name='delete_category'),
    url(r'^delete_indicator_from_category$', views.delete_indicator_from_category, name='delete_indicator_from_category'),
    url(r'^save_indicatorcategory$', views.save_indicatorcategory, name='save_indicatorcategory'),
    url(r'^save_categoryweight$', views.save_categoryweight, name='save_categoryweight'),

    url(r'^json_indicators$', views.json_indicators, name='json_indicators'),
    url(r'^json_categories_weight$', views.json_categories_weight, name='json_categories_weight'),
    url(r'^indicator/(?P<category_id>[0-9]+)$', views.json_indicator_in_category, name='json_indicator_in_category'),
]
