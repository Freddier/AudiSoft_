from types import SimpleNamespace
from django.test import TestCase
from apps.cruds.models import CategoryCRUD
from apps.dashboard.models import Category, IndicatorCategory, Indicator, \
							CategoryWeight, IndicatorType

class TestCategoryCRUD(TestCase):
	"""docstring for TestCategoryCRUD"""

	def setUp(self):
		IndicatorType.objects.create(key='qty', name='Cantidad')
		IndicatorType.objects.create(key='amount', name='Monto')

		indicator = Indicator.objects.create(code="DAC-TEST", name="Test Indicator", \
									description="Description for test indicator")

		category = Category.objects.create(name="Category 1", description="Category 1")

		IndicatorCategory.objects.create(category=category, indicator=indicator, weight=10, indicator_type_id='qty')


	def test_category_delete(self):
		query = Category.objects.filter(name="Category 1")
		exists = query.exists()
		self.assertTrue(exists)

		category = query.get()
		CategoryCRUD.delete_category(category.id)
		exists = query.exists()
		self.assertFalse(exists)

	def test_create_category(self):
		exists = Category.objects.filter(name="Test category").exists()
		self.assertFalse(exists)

		CategoryCRUD.create_category("Test category", "Test Category")
		exists = Category.objects.filter(name="Test category").exists()
		self.assertTrue(exists)

	def test_delete_indicator_from_category(self):

		query = IndicatorCategory.objects.filter(category__name='Category 1', indicator__code='DAC-TEST')
		exists = query.exists()
		self.assertTrue(exists)


		request = SimpleNamespace()
		indicatorcategory = query.get()
		request.body = '{ "indicator_id" : %s, "category_id" : %s }' % (indicatorcategory.indicator.id, indicatorcategory.category.id)
		res = CategoryCRUD.delete_indicator_from_category(request)
		self.assertTrue(res.success)

		exists = query.exists()
		self.assertFalse(exists)

	def test_add_indicator_to_category(self):
		indicator_id = 1
		category_id = 1

		request = SimpleNamespace()
		request.body = '{ "indicator_id" : %s, "category_id" : %s }' % (indicator_id, category_id)
		res = CategoryCRUD.add_indicator_to_category(request)
		self.assertTrue(res.success)

		query = IndicatorCategory.objects.filter(category_id=category_id, indicator_id=indicator_id)
		exists = query.exists()
		self.assertTrue(exists)



