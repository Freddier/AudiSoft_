from django.views.decorators.csrf import csrf_exempt
from http import HTTPStatus
from json import loads as json_decode

from AudISoft.extension import JsonResponse, dictfetchall, execute_sql, \
								select_fields_from_query, render
from AudISoft.strings import Strings
from apps.dashboard.models import Category, IndicatorCategory, Indicator, CategoryWeight
from apps.cruds.models import CategoryCRUD, Response

# define views
def index(request):
	return render(request,'cruds/index.html')

def category_indicator(request):
	return render(request,'cruds/category_indicator.html')

def answer_request(response: Response):
	success_response = JsonResponse(response.content, safe=False, status=HTTPStatus.OK)
	fail_response = JsonResponse(Strings.Response.GENERAL_BAD_RESPONSE, safe=False, status=HTTPStatus.INTERNAL_SERVER_ERROR)
	return success_response if response.success else fail_response

def delete_category(request, category_id):
	response = CategoryCRUD.delete_category(category_id)
	return answer_request(response)

def delete_indicator_from_category(request):
	response = CategoryCRUD.delete_indicator_from_category(request)
	return answer_request(response)

def add_indicator_to_category(request):
	response = CategoryCRUD.add_indicator_to_category(request)
	return answer_request(response)

def create_category(request):
	json_data = json_decode(request.body)
	response = CategoryCRUD.create_category(json_data['name'], json_data['description'])
	return answer_request(response)

def save_indicatorcategory(request):
	response = CategoryCRUD.save_indicatorcategory(request)
	return answer_request(response)

def json_categories_weight(request):
	response = CategoryCRUD.get_categories_weight()
	return answer_request(response)

def json_indicator_in_category(request, category_id):
	response = CategoryCRUD.indicators_in_category(category_id)
	return answer_request(response)

def save_categoryweight(request):
	response = CategoryCRUD.save_categoryweight(request)
	return answer_request(response)

def json_indicators(request):
	ls = select_fields_from_query(Indicator.objects.all(), ['id', 'name', 'code', 'description'])
	return JsonResponse(ls, safe=False)
