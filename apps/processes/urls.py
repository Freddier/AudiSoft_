from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^nodos$', views.root_nodes, name='root_nodes'),
    url(r'^nodos/(?P<parent_id>[0-9]+)$', views.child_nodes, name='child_nodes'),

    url(r'^$', views.processes, name='crud_processes'),
]
