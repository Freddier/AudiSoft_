from AudISoft.extension import render
from AudISoft.extension import JsonResponse, select_fields_from_query
from apps.processes.models import Node
from apps.dashboard.models import Category, Indicator
# Create your views here.

def root_nodes(request):
	return JsonResponse(Node.root(), safe=False)

def child_nodes(request, parent_id):
	return JsonResponse(list(Node.objects.filter(parent_id=parent_id).all()), safe=False)

def processes(request):
	categories = [{'id': category.id, 'name': category.name} for category in Category.objects.all()]
	indicators = [{'id': indicator.id, 'name': indicator.code + ' ' + indicator.name} for indicator in Indicator.objects.all()]
	nodes = Node.root()

	page_data = {
		'categories': categories,
		'indicators': indicators,
		'root_nodes': nodes
	}

	return render(request,'processes/index.html', page_data)
