# Generated by Django 2.0.1 on 2018-02-13 13:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('processes', '0002_auto_20180112_1041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='title',
            field=models.CharField(max_length=100),
        ),
    ]
