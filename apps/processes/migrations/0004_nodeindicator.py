# Generated by Django 2.0.1 on 2018-02-13 13:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_auto_20171226_1121'),
        ('processes', '0003_auto_20180213_0934'),
    ]

    operations = [
        migrations.CreateModel(
            name='NodeIndicator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weight', models.IntegerField()),
                ('indicator', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='dashboard.Indicator')),
                ('indicator_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='dashboard.IndicatorType', to_field='key')),
                ('node', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='processes.Node')),
            ],
        ),
    ]
