from django.db import models
from apps.dashboard.models import IndicatorType, Indicator
# Create your models here.

class NodeType(models.Model):
	title = models.CharField(max_length=50, null=False)

	def __str__(self):
		return self.title

class Node(models.Model):

	title = models.CharField(max_length=100, null=False)
	risk_level = models.DecimalField(max_digits=5, decimal_places=2, null=False)
	parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True)
	node_type = models.ForeignKey(NodeType , on_delete=models.CASCADE)

	def children(self):
		return Node.objects.filter(parent_id=self.id).all()

	@staticmethod
	def root():
		roots = Node.objects.filter(parent_id=None).all()
		for root in roots:
			root.children = list(root.children())
		return list(roots)

	def __str__(self):
		return self.title

	def __unicode__(self):
		return self.title


class NodeIndicator(models.Model):
	weight = models.IntegerField()
	indicator = models.ForeignKey(Indicator, on_delete=models.PROTECT)
	node = models.ForeignKey(Node, on_delete=models.PROTECT)
	indicator_type = models.ForeignKey(IndicatorType, to_field='key', on_delete=models.PROTECT);

	def __unicode__(self):
		return 'NodeIndicator(node="{0}",indicator="{1}", ind_type="{2}")'.format(self.node.title,
												self.indicator.name, self.indicator_type)

	def __str__(self):
		return self.__unicode__()
