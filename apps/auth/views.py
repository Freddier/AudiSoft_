from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as _login, logout as _logout

from AudISoft.extension import mainpage_if_logged
from AudISoft.strings import Strings
from apps.auth.models import AuthenticationBackend

# Create your views here.

def logout(request):
	_logout(request)
	return redirect('login')

@mainpage_if_logged
def login(request):
	context = {}
	if request.method == 'POST':
		# get form data
		form = request.POST
		# validate if user credentials are ok
		user = authenticate(username=form['username'], password=form['password'])
		if user is None:
			context['bad_login'] = True
		else:
			_login(request, user)
			AuthenticationBackend.set_default_session(request)
			return redirect('dashboard')

	return render(request,'auth/login.html', context)
