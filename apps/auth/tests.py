from base64 import b64decode
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.test import TestCase

from apps.auth.models import AuthenticationBackend
# Create your tests here.

class AuthTest(TestCase):
	"""docstring for TestAuth"""

	def setUp(self):
		user = User(username='test',password='test')
		user.save()

	def test_authenticate(self):
		success = authenticate(username='test', password='test')
		self.assertTrue(success)

	def test_validate_ldap(self):
		authenticationbackend = AuthenticationBackend()
		success = authenticationbackend.validate_ldap(username='fgabreu', password=b64decode(b'aW5jZXB0aW9u'))
		self.assertTrue(success)