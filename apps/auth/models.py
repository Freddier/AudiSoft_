from django.contrib.auth.models import User
from django.db import models
from django.shortcuts import get_object_or_404
from ldap3 import Server, Connection
from pprint import pprint

from AudISoft.strings import Strings
from apps.dashboard.models import Config


# Create your models here.
class AuthenticationBackend():
	'''Define custom authentication validating user with active directory'''

	def validate_ldap(self, username, password):
		'''Try to establish a connection with ldap '''
		try:
			server = Server('ldap://reservas.BRRD.com')
			connection = Connection(server, user='brrd\%s' % username , password=password)
			connection.open()
			return connection.bind()
		except Exception as ex:
			raise Exception('El servicio de ldap puede no estar funcionando adecuadamente.')

	def authenticate(self, request, username=None, password=None):
		user = User.objects.filter(username=username).first()
		# TODO enable ldap authentication
		# if self.validate_ldap(username, password):
		if user:
			return user
		else:
			return None

	def get_user(self, user_id):
		try:
			return User.objects.get(pk=user_id)
		except User.DoesNotExist:
			return None

	@staticmethod
	def set_default_session(request):
		config_variables = Config.objects.all()
		config = {}
		for setting in config_variables:
			config[setting.key] = setting.value

		request.session[Strings.USER_SETTINGS] = config

